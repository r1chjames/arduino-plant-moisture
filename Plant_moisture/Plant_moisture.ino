#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <Adafruit_Sensor.h>
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>

#define mqtt_server "192.168.0.58"
#define mqtt_user "homeassistant"
#define mqtt_password "2032"
#define mqtt_client "plant_dragon_tree"
#define SLEEP_DELAY_IN_SECONDS  21600
#define substrate_moisture_topic "sensor/plants/plant_dragon_soil_moisture"

//Pins
const int soil = A0;
const int power = D6;

//Timer
const int TIMER = 600; // in seconds
const int RESET = TIMER - 1;
const int SENSOR_DELAY = 10; // in milliseconds

WiFiClient espClient;
PubSubClient client(espClient);

void setup(void) 
{
  pinMode(soil, INPUT);
  pinMode(power, OUTPUT);
  digitalWrite(power, LOW);
    
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
}

void setup_wifi() {
  WiFiManager wifiManager;
  wifiManager.autoConnect(mqtt_client);
}

void reconnect() {
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection... ");
    if (client.connect(mqtt_client, mqtt_user, mqtt_password)) {
      Serial.println("connected");
    } else {
      Serial.print("Failed to connect, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

bool checkBound(float newValue, float prevValue, float maxDiff) {
  return !isnan(newValue) &&
         (newValue < prevValue - maxDiff || newValue > prevValue + maxDiff);
}

double analogValue = 0.0;
int relativeMoisture = 0;
int moistureDiff = 1;
void readMoisture() {

  analogValue = analogRead(soil);
  
  //tune this number so that when dry, the relative moisture ends up being 0
  int newRelativeMoisture = (analogValue * 100) / 966;

  // now reverse the value so that the value goes up as moisture increases
  newRelativeMoisture = 100 - newRelativeMoisture;

  if (checkBound(newRelativeMoisture, relativeMoisture, moistureDiff)) {
    relativeMoisture = newRelativeMoisture;
    
    Serial.print("Analog Raw Value: ");
    Serial.println(analogValue);
    Serial.print("Relative Moisture: ");
    Serial.println(relativeMoisture);
    Serial.println(" ");    
    Serial.println(String(relativeMoisture).c_str());
    
    client.publish(substrate_moisture_topic, String(relativeMoisture).c_str(), true);
  }
}

void takeMessage() {
  // Turn sensor ON and wait a moment.
  digitalWrite(power, HIGH);
  delay(SENSOR_DELAY);

  readMoisture();

  // Turn sensor OFF again.
  digitalWrite(power, LOW);
  delay(1000 - SENSOR_DELAY);
}

void loop() {  
  if (!client.connected()) {
    Serial.print("Not connected to MQTT. State: ");
    Serial.println(client.state());
    reconnect();
  }
  client.loop();
  takeMessage();
  
  Serial.print("Entering deep sleep mode for ");
  Serial.print(SLEEP_DELAY_IN_SECONDS);
  Serial.println(" seconds...");
  ESP.deepSleep(SLEEP_DELAY_IN_SECONDS * 1000000, WAKE_RF_DEFAULT);
  delay(500);
}
