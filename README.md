# Arduino Soil Moisture Sensor

Arduino IDE project for ESP8266 to take readings from a FC-28 analog soil moisture sensor and publish to MQTT.

## Information

The specifications of the FC-28 soil moisture sensor  are as follows:
- Input Voltage: 3.3 – 5V
- Output Voltage: 0 – 4.2V
- Input Current: 35mA
- Output Signal: Both Analog and Digital

The sketch uses WifiManager to connect to Wifi.

## Hookup
The sketch uses a digital pin to provide power instead of the standard 3V3 pin as the electrodes corrode quickly when powered continuously. Using a digital pin allows power to be turned off when the sensor is not being polled.

### Pins:
| ESP8266 | FC-28 |
| ------- | ----- |
| D6      | VIN   |
| A0      | A0    |
| GND     | GND   |


## Libraries
- WifiManager
- PubSubClient
- Adafruit Sensor
- ESP8266Wifi
